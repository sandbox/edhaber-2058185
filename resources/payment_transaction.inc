<?php

/**
 * @file
 * Contains the resource callbacks for orders.
 */


/**
 * Determines an API user's access to index payments.
 */
function commerce_paytrace_services_payment_transaction_index_access() {
  // Because the entity access control system will filter any result sets,
  // there's no reason to limit a particular user's access. Users with the
  // administer payment permission will have access to view any payment but
  // other users will only have access to payments on orders they can view.
  return TRUE;
}

/**
 * Returns a single payment.
 *
 * @see commerce_services_retrieve_entity()
 */
function commerce_paytrace_services_payment_transaction_retrieve($payment_id, $expand_entities, $flatten_fields) {
  return commerce_services_retrieve_entity('commerce_payment_transaction', $payment_id, $expand_entities, $flatten_fields);
}

/**
 * Determines an API user's access to retrieve a given payment.
 *
 * @param $payment_id
 *   The ID of the payment to be retrieved.
 *
 * @return
 *   Boolean indicating the user's access to retrieve the payment.
 */
function commerce_paytrace_services_payment_transaction_retrieve_access($payment_id) {
  // Attempt to load the payment.
  if ($payment = commerce_payment_transaction_load($payment_id)) {
    // And perform the view access check.
    if (commerce_payment_transaction_access('view', $payment)) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }
  }
  else {
    return services_error(t('Payment not found'), 404);
  }
}

/**
 * Creates a new payment on an order.
 */
function commerce_paytrace_services_payment_transaction_create($data, $flatten_fields) {
  // Ensure the create request specifies a valid payment type.
  if (empty($data['type']) || !in_array($data['type'], array_keys(commerce_payment_transaction_types()))) {
    return services_error(t('You must specify a valid payment type'), 400);
  }

  // Default the quantity to 1.
  //if (empty($data['quantity'])) {
  //  $data['quantity'] = 1;
  //}

  // Create the new payment.
  $payment = commerce_payment_transaction_new($data['type'], $data['order_id']);

  // Remove the type and order_id from the data array since they've been set.
  unset($data['type'], $data['order_id']);

  // If the payment is a product payment, ensure a commerce_product value
  // was passed referencing a valid product ID and populate the payment.
  if (in_array($payment->type, commerce_product_payment_types())) {
    $product = NULL;

    if (!empty($data['commerce_product'])) {
      if ($flatten_fields == 'true') {
        $product = commerce_product_load($data['commerce_product']);
      }
      else {
        $product = commerce_product_load($data['commerce_product']['und'][0]);
      }
    }

    // Bail now if the product wasn't found.
    if (empty($product)) {
      return services_error(t('You must specify a valid commerce_product value'), 400);
    }

    // Populate the payment.
    commerce_product_payment_populate($payment, $product);
  }

  // Set the field and property data and save the payment.
  commerce_services_set_field_values('commerce_payment_transaction', $payment, $data, $flatten_fields);
  commerce_payment_transaction_save($payment);

  // Add the payment to its order.
  $order = commerce_order_load($payment->order_id);
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $order_wrapper->commerce_payment_transactions[] = $payment;
  commerce_order_save($order);

  // Add simplified fields to the payment object for certain field types.
  commerce_services_decorate_entity('commerce_payment_transaction', $payment);

  // Flatten field value arrays if specified. This must be the last operation
  // performed as it breaks the standard field data model. An entity whose
  // fields have thus been flattened is no longer wrappable or writable.
  if ($flatten_fields == 'true') {
    $payment = clone($payment);
    commerce_services_flatten_fields('commerce_payment_transaction', $payment);
  }

  return $payment;
}

/**
 * Determines an API user's access to create new payments.
 */
function commerce_paytrace_services_payment_transaction_create_access($data) {
  // Load the order from the data array to determine access to create payments
  // on it.
  $order = NULL;

  if (!empty($data['order_id'])) {
    $order = commerce_order_load($data['order_id']);
  }

  if (empty($order)) {
    return services_error(t('You must specify a valid order ID', 400));
  }

  // If the user has access to update the order...
  if (commerce_order_access('update', $order)) {
    return TRUE;
  }
  else {
    return services_error(t('Access to this operation not granted'), 401);
  }
}

/*
*/
function _commerce_paytrace_services_order_payment_authorize($order_id, $card_id, $amount, $currency_code){
  module_load_include('module', 'commerce_cardonfile');
  $card = commerce_cardonfile_load($card_id);
  $order = commerce_order_load($order_id);
  watchdog('commerce_paytrace_services', 'Capture _commerce_paytrace_services_order_payment_authorize order id @order_id, card id @card_id, amount @amount, currency_code @currency_code', array('@order_id' => $order_id,'@card_id'=> $card_id, '@amount' => $amount,'@currency_code' => $currency_code, WATCHDOG_DEBUG));

  if (!empty($card) && !empty($order)) {
    $charge = array();
    if($amount > 0){
      $charge = array('amount' => $amount, 'currency_code' => $currency_code);
    }
    $cof_response = commerce_cardonfile_order_charge_card($order, $charge, $card);
    if (!empty($cof_response['status'])) {
      global $user;
      watchdog(
        'commerce_paytrace_services',
        'User @user_name(@user_id) has charged card id #@card_id for order #@order_id using the terminal.',
        array(
          '@user_name' => $user->name,
          '@user_id' => $user->uid,
          '@card_id' => $card->card_id,
          '@order_id' => $order->order_number,
        )
      );
      return (object)array('status'=>'success','response' => $cof_response);
    }
  }

  return (object)array('status'=>'failure','response' => $cof_response);
}



function _commerce_paytrace_services_payment_transaction_capture($payment_id, $amount){
  // Load the payment.
  
  $transaction = commerce_payment_transaction_load($payment_id);
  watchdog('commerce_paytrace_services', 'Capture _commerce_paytrace_services_payment_transaction_capture payment id @payment_id, amount @amount', array('@payment_id' => $payment_id, '@amount' => $amount, WATCHDOG_DEBUG));

  //watchdog('commerce_paytrace_services', 'Processing stored card  @card_data', array('@card_data' => '<pre>' . check_plain(print_r($card_data, TRUE)) . '</pre>', WATCHDOG_DEBUG));

  $amount = number_format($amount/100, 2, '.', '');

  // Build a name-value pair array for this transaction.
  $txn_type = COMMERCE_CREDIT_PRIOR_AUTH_CAPTURE;
  $nvp = array(
    'TRANXTYPE' => commerce_paytrace_txn_type($txn_type),
    'METHOD' => 'ProcessTranx',
    'TRANXID' => $transaction->remote_id,
    'AMOUNT' => $amount,
  );

  // Submit the request to Paytrace.
  $payment_method = commerce_payment_method_instance_load($transaction->instance_id);
  $response = commerce_paytrace_request($payment_method, $nvp);

  // Update and save the transaction based on the response.
  $transaction->payload[REQUEST_TIME] = $response;

  $success = FALSE;
  // If we didn't get an approval response code...
  if ($response->error) {
    // Create a failed transaction with the error message.
    $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
    //t('Prior authorization capture failed, so the transaction will remain in a pending status.'), 'error');
    //drupal_set_message(check_plain($response->error_message), 'error');
  }
  else {
    // For a capture, check for correct response code.
    if (!$response->captured) {
      //drupal_set_message(t('Prior authorization capture failed, so the transaction will remain in a pending status.'), 'error');
      //drupal_set_message(check_plain($response->approval_message), 'error');
    }
    else {
      //drupal_set_message(t('Prior authorization captured successfully.'));

      // Update the transaction amount to the actual capture amount.
      $transaction->amount = commerce_currency_decimal_to_amount($amount, $transaction->currency_code);
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->remote_status = $txn_type; //$response[11];

      // Append a capture indication to the result message.
      $transaction->message .= '<br />' . t('<strong>CAPTURED:</strong> @date', array('@date' => format_date(REQUEST_TIME, 'short')));
    }
  }

  commerce_payment_transaction_save($transaction);
  //commerce_services_retrieve_entity('commerce_payment_transaction', $payment_id, 1, true);
  //echo "TEST $payment_id $amount";
  //exit();
  return $transaction;
}

/**
 * Determines an API user's access to update a given payment.
 *
 * @param $payment_id
 *   The ID of the payment to be updated.
 *
 * @return
 *   Boolean indicating the user's access to update the payment.
 */
function commerce_paytrace_services_payment_transaction_capture_access($payment_id) {
  // Attempt to load the payment.
  if ($payment = commerce_payment_transaction_load($payment_id)) {
    $order = commerce_order_load($payment->order_id);
    // If the user has access to perform the operation...
    
    if (commerce_paytrace_capture_access($order, $payment)) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }
  }
  else {
    return services_error(t('Payment not found'), 404);
  }
}


/**
 * Determines an API user's access to update a given payment.
 *
 * @param $payment_id
 *   The ID of the payment to be updated.
 *
 * @return
 *   Boolean indicating the user's access to update the payment.
 */
function commerce_paytrace_services_order_payment_authorize_access($order_id) {
  // Attempt to load the order.
  if ($order = commerce_order_load($order_id)) {
    
    // If the user has access to perform the operation...
    
    if (commerce_payment_transaction_order_access('create', $order)) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }
  }
  else {
    return services_error(t('Order not found'), 404);
  }
}

/**
 * Determines an API user's access to update a given payment.
 *
 * @param $payment_id
 *   The ID of the payment to be updated.
 *
 * @return
 *   Boolean indicating the user's access to update the payment.
 */
function commerce_paytrace_services_payment_transaction_update_access($payment_id) {
  // Attempt to load the payment.
  if ($payment = commerce_payment_transaction_load($payment_id)) {
    // If the user has access to perform the operation...
    print_r($payment);exit();
    if (commerce_payment_transaction_access('update', $payment)) {
      return TRUE;
    }
    else {
      return services_error(t('Access to this operation not granted'), 401);
    }
  }
  else {
    return services_error(t('Payment not found'), 404);
  }
}
